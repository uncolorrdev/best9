package com.app.bestnine;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amplitude.api.Amplitude;
import com.app.bestnine.adapter.PhotosAdapter;
import com.app.bestnine.application.AppPermissionManager;
import com.app.bestnine.models.UserInfo;
import com.app.bestnine.utils.GradientRandom;
import com.app.bestnine.utils.ScreenshotMaker;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ResultsActivity extends AppCompatActivity {

    private static final String ARG_USER_INFO = "user_info";
    private static final String ARG_USERNAME = "username";

    private static final int MODE_BY_LIKES = 1;
    private static final int MODE_BY_COMMENTS = 2;

    @BindView(R.id.textViewLikesCount)
    TextView textViewLikesCount;

    @BindView(R.id.textViewCommentsCount)
    TextView textViewCommentsCount;

    @BindView(R.id.textViewPostsCount)
    TextView textViewPostsCount;

    @BindView(R.id.textViewUsername)
    TextView textViewUsername;

    @BindView(R.id.imageViewProfile)
    CircleImageView imageViewProfile;

    @BindView(R.id.recyclerViewPhotos)
    RecyclerView recyclerViewPhotos;

    @BindView(R.id.buttonByLikes)
    Button buttonByLikes;

    @BindView(R.id.buttonByComments)
    Button buttonByComments;

    @BindView(R.id.buttonImprove2020)
    Button buttonImprove2020;

    @BindView(R.id.buttonShare)
    Button buttonShare;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private PhotosAdapter adapter;

    private UserInfo userInfo;

    private String username;

    private BottomSheetBehavior bottomSheetBehavior;

    private ScreenshotMaker screenshotMaker = new ScreenshotMaker(this);

    public static Intent getInstance(Context context, UserInfo userInfo, String username) {
        Intent intent = new Intent(context, ResultsActivity.class);
        intent.putExtra(ARG_USER_INFO, userInfo);
        intent.putExtra(ARG_USERNAME, username);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ButterKnife.bind(this);
        coordinatorLayout.setBackgroundResource(GradientRandom.getGradient());
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        userInfo = (UserInfo) getIntent().getSerializableExtra(ARG_USER_INFO);
        username = getIntent().getStringExtra(ARG_USERNAME);

        adapter = new PhotosAdapter();
        recyclerViewPhotos.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewPhotos.setAdapter(adapter);
        fillUserInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppPermissionManager.PERMISSION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                readyForScreenshot();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Amplitude.getInstance().logEvent("Open MainVC");
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            Amplitude.getInstance().logEvent("Close MainVC");
            finish();
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

    }

    @OnClick(R.id.imageButtonBack)
    void onBackButtonClick() {
        Amplitude.getInstance().logEvent("Close MainVC");
        finish();
    }

    @OnClick(R.id.imageButtonSettings)
    void onSettingsButtonClick() {
        startActivity(SettingsActivity.getInstance(this));
    }

    @OnClick(R.id.buttonByLikes)
    void onButtonByLikesClick() {
        try {
            Amplitude.getInstance().logEvent("Change filter",
                    new JSONObject("{\"segment_select\":\"0\"}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        buttonByLikes.setBackgroundResource(R.drawable.buttons_mode_drawable);
        buttonByComments.setBackgroundResource(android.R.color.transparent);
        fillPhotos(MODE_BY_LIKES);
    }

    @OnClick(R.id.buttonByComments)
    void onButtonByCommentsClick() {
        try {
            Amplitude.getInstance().logEvent("Change filter",
                    new JSONObject("{\"segment_select\":\"1\"}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        buttonByLikes.setBackgroundResource(android.R.color.transparent);
        buttonByComments.setBackgroundResource(R.drawable.buttons_mode_drawable);
        fillPhotos(MODE_BY_COMMENTS);
    }

    @OnClick(R.id.buttonShare)
    void onButtonShareClick() {
        Amplitude.getInstance().logEvent("Share action");
        if (AppPermissionManager.checkIfAlreadyHavePermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            readyForScreenshot();
        } else {
            AppPermissionManager.requestAppPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    AppPermissionManager.PERMISSION_REQUEST_CODE);
        }
    }

    @OnClick(R.id.buttonSendToStories)
    void onButtonSendToStoriesClick() {
        screenshotMaker.createFile(screenshotMaker.getScreenshotBitmap(), false);
        if (screenshotMaker.getUri() == null) {
            return;
        }
        try {
            Amplitude.getInstance().logEvent("Share",
                    new JSONObject("{\"to\":\"instagram\"}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String attributionLinkUrl = "https://best9.app/";

        Intent intent = new Intent("com.instagram.share.ADD_TO_STORY");
        intent.setDataAndType(screenshotMaker.getUri(), "image/jpeg");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra("content_url", attributionLinkUrl);

        if (getPackageManager().resolveActivity(intent, 0) != null) {
            startActivityForResult(intent, 0);
        }
    }

    @OnClick(R.id.buttonSaveToPhoto)
    void onButtonSaveToPhotoClick() {
        try {
            Amplitude.getInstance().logEvent("Share",
                    new JSONObject("{\"to\":\"photolibrary\"}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (screenshotMaker.getScreenshotBitmap() != null) {
            screenshotMaker.createFile(screenshotMaker.getScreenshotBitmap(), true);
        }
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @OnClick(R.id.buttonMore)
    void onButtonMoreClick() {
        screenshotMaker.createFile(screenshotMaker.getScreenshotBitmap(), false);
        try {
            Amplitude.getInstance().logEvent("Share",
                    new JSONObject("{\"to\":\"more\"}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setDataAndType(screenshotMaker.getUri(), "image/*");
        intent.putExtra(Intent.EXTRA_STREAM, screenshotMaker.getUri());
        intent.putExtra(Intent.EXTRA_TEXT, "Share From best9.app");
        startActivity(Intent.createChooser(intent, "Share image using"));
    }

    @OnClick(R.id.buttonImprove2020)
    void onButtonImprove2020Click() {
        Amplitude.getInstance().logEvent("Go to improve");
        String url = "http://api.best9.app/referal?platform=android";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.buttonCancel)
    void onButtonCancelClick() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @SuppressLint("SetTextI18n")
    private void fillUserInfo() {
        if(!userInfo.getProfileUrl().isEmpty()){
            Picasso.get().load(userInfo.getProfileUrl()).into(imageViewProfile);
        }
        fillPhotos(MODE_BY_LIKES);
        textViewUsername.setText(username);
        textViewCommentsCount.setText(Integer.toString(userInfo.getCommentsCount()));
        textViewLikesCount.setText(Integer.toString(userInfo.getLikesCount()));
        textViewPostsCount.setText(Integer.toString(userInfo.getPostsCount()));
    }

    public void fillPhotos(int mode) {
        switch (mode) {
            case MODE_BY_LIKES:
                adapter.update(userInfo.getByLikes());
                break;
            case MODE_BY_COMMENTS:
                adapter.update(userInfo.getByComments());
                break;
        }
    }

    private void readyForScreenshot() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        buttonByComments.setVisibility(View.INVISIBLE);
        buttonByLikes.setVisibility(View.INVISIBLE);
        buttonShare.setVisibility(View.INVISIBLE);
        buttonImprove2020.setVisibility(View.INVISIBLE);
        toolbar.setVisibility(View.GONE);
        scrollView.scrollTo(0, 0);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            screenshotMaker.makeScreenshotBitmap(decorView.getRootView());
            showUnnecessaryForScreenshot();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }, 1000);
    }

    private void showUnnecessaryForScreenshot() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);
        toolbar.setVisibility(View.VISIBLE);
        buttonByComments.setVisibility(View.VISIBLE);
        buttonByLikes.setVisibility(View.VISIBLE);
        buttonShare.setVisibility(View.VISIBLE);
        buttonImprove2020.setVisibility(View.VISIBLE);
    }
}
