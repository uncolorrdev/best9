package com.app.bestnine.Api;

import com.app.bestnine.models.StatResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiSource {

    @GET("stat")
    Single<StatResponse> getStat(@Query("username") String username);
}
