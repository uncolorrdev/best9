package com.app.bestnine.application;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.amplitude.api.Amplitude;
import com.app.bestnine.Api.Api;
import com.google.android.gms.ads.MobileAds;

public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Api.init();
        Amplitude.getInstance().initialize(this, "5e37d066a3e992fb734fb01faeba433f")
                .enableForegroundTracking(this);
        MobileAds.initialize(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static void Log(String message) {
        if (!message.isEmpty()) {
            Log.i("fg", message);
        }

    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }
}
