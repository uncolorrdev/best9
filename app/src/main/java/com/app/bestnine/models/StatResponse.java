package com.app.bestnine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private boolean isSuccess;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("result")
    @Expose
    private UserInfo userInfo;

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getError() {
        return error;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public boolean isErrorEmpty(){
        return error.isEmpty();
    }
}
