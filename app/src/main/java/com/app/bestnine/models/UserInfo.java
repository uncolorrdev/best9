package com.app.bestnine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserInfo implements Serializable {

    @SerializedName("likes_count")
    @Expose
    private int likesCount;

    @SerializedName("comments_count")
    @Expose
    private int commentsCount;

    @SerializedName("posts_count")
    @Expose
    private int postsCount;

    @SerializedName("by_likes")
    @Expose
    private List<PhotoModel> byLikes;

    @SerializedName("by_comments")
    @Expose
    private List<PhotoModel> byComments;

    @SerializedName("profile_pic_src")
    @Expose
    private String profileUrl;

    public String getProfileUrl() {
        return profileUrl;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public int getPostsCount() {
        return postsCount;
    }

    public List<PhotoModel> getByLikes() {
        return byLikes;
    }

    public List<PhotoModel> getByComments() {
        return byComments;
    }
}
