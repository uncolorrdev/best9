package com.app.bestnine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PhotoModel implements Serializable {

    @SerializedName("src")
    @Expose
    private String src;

    @SerializedName("likes_count")
    @Expose
    private int likesCount;

    @SerializedName("comments_count")
    @Expose
    private int commentsCount;


    public String getSrc() {
        return src;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }
}
