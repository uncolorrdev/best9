package com.app.bestnine;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.adView)
    AdView adView;

    public static Intent getInstance(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @OnClick(R.id.imageButtonBack)
    void onBackButtonClick(){
        finish();
    }

    @OnClick(R.id.buttonContactUs)
    void onContactUsButtonClick(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","support@lwts.ru", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ "support@lwts.ru"});
        startActivity(Intent.createChooser(emailIntent, getString(R.string.email_choose)));
    }

    @OnClick(R.id.buttonOutWebsite)
    void onOutWebsiteButtonClick(){
        String url = "https://best9.app/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.buttonDevelopedByLWTS)
    void onDevelopedButtonClick(){
        String url = "https://lwts.ru/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    @OnClick(R.id.buttonPrivacyPolicy)
    void onPrivacyPolicyButtonClick(){
        String url = "https://best9.app/pp/pp.html";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}