package com.app.bestnine.search_activity;

import android.content.Context;

import com.amplitude.api.Amplitude;
import com.app.bestnine.Api.Api;
import com.app.bestnine.R;
import com.app.bestnine.application.App;
import com.app.bestnine.models.StatResponse;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SearchActivityPresenter implements SearchActivityContract.Presenter {

    private Context context;

    private SearchActivityContract.View view;

    public SearchActivityPresenter(Context context, SearchActivityContract.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getStat(String username) {
        view.showProcessing();
        Api.getSource().getStat(username).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<StatResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(StatResponse result) {
                        view.hideProcessing();
                        if (result == null) {
                            try {
                                Amplitude.getInstance().logEvent("End search",
                                        new JSONObject("{\"error\":\"Unknown error\"}"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            view.showMessage(context.getString(R.string.title_message_error),
                                    context.getString(R.string.message_unknown_error));
                            return;
                        }

                        if (!result.isErrorEmpty()) {
                            try {
                                Amplitude.getInstance().logEvent("End search",
                                        new JSONObject("{\"error\":\"" + result.getError() + "\"}"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            App.Log(result.getError());
                            view.showMessage(context.getString(R.string.title_message_error),
                                    result.getError());
                            return;
                        }

                        if (result.isSuccess()) {
                            view.showResults(result.getUserInfo());
                            try {
                                Amplitude.getInstance().logEvent("End search",
                                        new JSONObject("{\"success\":\"yeah\"}"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProcessing();
                        try {
                            Amplitude.getInstance().logEvent("End search",
                                    new JSONObject("{\"error\":\"Unknown error\"}"));
                        } catch (JSONException jsonException) {
                            jsonException.printStackTrace();
                        }
                        view.showMessage(context.getString(R.string.title_message_error),
                                context.getString(R.string.message_unknown_error));

                    }
                });
    }
}
