package com.app.bestnine.search_activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.amplitude.api.Amplitude;
import com.app.bestnine.R;
import com.app.bestnine.ResultsActivity;
import com.app.bestnine.models.UserInfo;
import com.app.bestnine.utils.LoadingDialog;
import com.app.bestnine.utils.MessageReporter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity implements SearchActivityContract.View {

    @BindView(R.id.editTextUsername)
    EditText editTextUsername;

    private AlertDialog dialogProcessing;

    private SearchActivityPresenter presenter;

    private InterstitialAd interstitialAd;

    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        dialogProcessing = LoadingDialog.newInstanceWithoutCancelable(this);
        presenter = new SearchActivityPresenter(this, this);
        interstitialAd = new InterstitialAd(this);
        initAds();
    }

    private void initAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-1641691315583385/7647271520");

        //interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // interstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                interstitialAd.loadAd(new AdRequest.Builder().build());
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                startActivity(ResultsActivity.getInstance(SearchActivity.this, userInfo,
                        editTextUsername.getText().toString()));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        interstitialAd.loadAd(new AdRequest.Builder().build());
        Amplitude.getInstance().logEvent("Open AuthVC");
    }

    @OnClick(R.id.buttonContinue)
    void onButtonContinueClick() {
        if (editTextUsername.getText().toString().isEmpty()) {
            return;
        }
        Amplitude.getInstance().logEvent("Click to search");
        presenter.getStat(editTextUsername.getText().toString());
    }


    @OnClick(R.id.buttonPolicy)
    void onButtonPolicyClick() {
        String url = "https://best9.app/pp/pp.html";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void showProcessing() {
        dialogProcessing.show();
    }

    @Override
    public void hideProcessing() {
        dialogProcessing.hide();
    }

    @Override
    public void showMessage(String title, String message) {
        MessageReporter.showMessage(this, title, message);
    }

    @Override
    public void showResults(UserInfo userInfo) {
        this.userInfo = userInfo;
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            startActivity(ResultsActivity.getInstance(this, userInfo,
                    editTextUsername.getText().toString()));
        }

    }
}
