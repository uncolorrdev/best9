package com.app.bestnine.search_activity;

import com.app.bestnine.models.UserInfo;

public interface SearchActivityContract {

    interface View {
        void showProcessing();
        void hideProcessing();
        void showMessage(String title, String message);
        void showResults(UserInfo userInfo);
    }

    interface Presenter {
        void getStat(String username);
    }
}
