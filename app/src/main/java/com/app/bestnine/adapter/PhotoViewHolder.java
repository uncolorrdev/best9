package com.app.bestnine.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.bestnine.R;
import com.app.bestnine.models.PhotoModel;
import com.app.bestnine.widgets.SquareImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoViewHolder extends RecyclerView.ViewHolder {


    private SquareImageView imageViewPhoto;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imageViewFailure)
    ImageView imageViewFailure;

    private boolean isLoaded = true;

    private String src;

    public PhotoViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        imageViewPhoto = itemView.findViewById(R.id.imageViewPhoto);
    }

    public void bind(PhotoModel photoModel) {
        src = photoModel.getSrc();

        uploadPhoto(src);
    }

    private void uploadPhoto(String src) {
        Picasso.get()
                .load(src)
                .into(imageViewPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                        imageViewFailure.setVisibility(View.GONE);
                        isLoaded = true;

                    }

                    @Override
                    public void onError(Exception e) {
                        isLoaded = false;
                        imageViewFailure.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }

                });
    }

    @OnClick(R.id.imageViewPhoto)
    void onImageViewPhotoClick() {
        if (isLoaded) {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        imageViewFailure.setVisibility(View.GONE);
        isLoaded = true;
        uploadPhoto(src);


    }
}
