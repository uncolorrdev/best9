package com.app.bestnine.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.View;

import androidx.core.content.FileProvider;

import com.app.bestnine.BuildConfig;
import com.app.bestnine.application.App;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class ScreenshotMaker {

    private Bitmap screenshotBitmap;

    private Uri uri;

    private Context context;

    public ScreenshotMaker(Context context) {
        this.context = context;
    }

    public Bitmap makeScreenshotBitmap(View rootView) {
        try {
            //View rootView = getWindow().getDecorView().getRootView();
            screenshotBitmap = getBitmapFromView(rootView);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return screenshotBitmap;
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public Bitmap getScreenshotBitmap() {
        return screenshotBitmap;
    }

    public void createFile(Bitmap bitmap, boolean inGallery) {
        Date now = new Date();
        DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        String path = null;
        if(inGallery){
            //path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/" + now + ".jpg";

            MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, now.toString() , "");
            return;
        }
        else {
            path = context.getExternalFilesDir(null) + "/" + now + ".jpg";
            App.Log(path);
        }

        try {
            File imageFile = new File(path);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Uri getUri() {
        return uri;
    }
}
