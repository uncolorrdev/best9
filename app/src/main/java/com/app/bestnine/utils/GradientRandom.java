package com.app.bestnine.utils;

import com.app.bestnine.R;

import java.util.Random;

public class GradientRandom {

    public static int[] gradients = new int[]{R.drawable.gradient1,
            R.drawable.gradient2,
            R.drawable.gradient3,
            R.drawable.gradient4,
            R.drawable.gradient5,
            R.drawable.gradient6,
            R.drawable.gradient7,
            R.drawable.gradient8,
            R.drawable.gradient9,
            R.drawable.gradient10};

    public static int getGradient(){
        Random random = new Random();
        int index = random.nextInt(gradients.length - 1);
        return gradients[index];
    }
}
